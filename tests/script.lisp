(in-package :cl-user)
(let* ((string-stream (make-string-output-stream))
       (*standard-output*
         (make-broadcast-stream *standard-output*
                                string-stream)))
  (ql:quickload :jsonrpc/tests)
  (asdf:test-system "jsonrpc")
  (let ((string (get-output-stream-string string-stream)))
    (uiop:quit (if (search "failed" string) 1 0))))

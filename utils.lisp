(in-package #:cl-user)
(defpackage #:jsonrpc/utils
  (:use #:cl)
  (:import-from #:usocket
                #:socket-listen
                #:socket-close
                #:address-in-use-error)
  (:import-from #:bordeaux-threads
                #:make-lock
                #:with-lock-held
                #:destroy-thread)
  (:export #:random-port
           #:make-id
           #:find-mode-class
           #:destroy-thread*
           #:without-interrupts
           #:unwind-protect*))
(in-package #:jsonrpc/utils)

(defun port-available-p (port)
  (handler-case (let ((socket (usocket:socket-listen "127.0.0.1" port :reuse-address t)))
                  (usocket:socket-close socket))
    (usocket:address-in-use-error (e) (declare (ignore e)) nil)))

(defun random-port ()
  "Return a port number not in use from 50000 to 60000."
  (loop for port from (+ 50000 (random 1000)) upto 60000
        if (port-available-p port)
          return port))

(defun make-id (&optional (length 12))
  (declare (type fixnum length))
  (let ((result (make-string length)))
    (declare (type simple-string result))
    (dotimes (i length result)
      (setf (aref result i)
            (ecase (random 5)
              ((0 1) (code-char (+ #.(char-code #\a) (random 26))))
              ((2 3) (code-char (+ #.(char-code #\A) (random 26))))
              ((4) (code-char (+ #.(char-code #\0) (random 10)))))))))

(defgeneric find-mode-class (mode))

(defun destroy-thread* (thread)
  (handler-case
      (bt:destroy-thread thread)
    ((or #+sbcl sb-thread:interrupt-thread-error) ())))

(defmacro without-interrupts (&body body)
  #+sbcl `(sb-sys:without-interrupts ,@body)
  #+ccl `(ccl:without-interrupts ,@body)
  #-(or sbcl ccl)
  `(progn ,@body))

(defmacro unwind-protect* (form &body cleanup-forms)
  #+sbcl
  `(unwind-protect
        ,form
     (without-interrupts
       ,@cleanup-forms))
  ;; In CCL the cleanup forms of an unwind-protect have an implicit
  ;; without-interrupts.
  #-sbcl
  `(unwind-protect
        ,form
     ,@cleanup-forms))

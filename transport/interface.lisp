(in-package #:cl-user)
(defpackage #:jsonrpc/transport/interface
  (:use #:cl)
  (:import-from #:jsonrpc/connection
                #:wait-for-ready
                #:process-request
                #:add-message-to-queue
                #:connection-request-queue
                #:connection-outbox
                #:add-message-to-outbox
                #:connection-socket)
  (:import-from #:usocket)
  (:import-from #:bordeaux-threads)
  (:import-from #:event-emitter
                #:event-emitter)
  (:import-from #:chanl)
  (:import-from #:alexandria
                #:ignore-some-conditions
                #:when-let)
  (:export #:transport
           #:transport-message-callback
           #:transport-connection
           #:transport-threads
           #:start-server
           #:start-client
           #:send-message-using-transport
           #:receive-message-using-transport
           #:run-processing-loop
           #:run-reading-loop
           #:transport-jsonrpc))
(in-package #:jsonrpc/transport/interface)

(defclass transport (event-emitter)
  ((message-callback :initarg :message-callback
                     :accessor transport-message-callback)
   (connection :accessor transport-connection)
   (threads :initform '()
            :accessor transport-threads)
   (jsonrpc :initarg :client
            :initarg :server
            :accessor transport-jsonrpc)))

(defmethod (setf transport-connection) :around ((value null) self)
  (let ((connection (transport-connection self)))
    (multiple-value-prog1 (call-next-method)
      (when-let (socket (connection-socket connection))
        (typecase socket
          (usocket:usocket
           (usocket:socket-close socket))
          (stream
           (ignore-some-conditions (stream-error)
             (finish-output socket))
           (ignore-some-conditions (stream-error)
             (close socket))))))))

(defgeneric start-server (transport))

(defgeneric start-client (transport))

(defgeneric send-message-using-transport (transport to message))

(defgeneric receive-message-using-transport (transport from))

(defgeneric run-processing-loop (transport connection)
  (:method ((transport transport) connection)
    (let ((request-queue (connection-request-queue connection))
          (outbox (connection-outbox connection)))
      (flet ((blocked? ()
               (and (chanl:recv-blocks-p request-queue)
                    (chanl:recv-blocks-p outbox))))
        (handler-bind ((type-error
                         ;; Broken pipe.
                         (lambda (e)
                           (declare (ignorable e))
                           #+sbcl
                           (when (and (null (type-error-datum e))
                                      (eql (type-error-expected-type e)
                                           'sb-impl::buffer))
                             (return-from run-processing-loop nil)))))
          (handler-case
              (loop
                (tagbody
                 :wakeup
                   ;; Spurious wakeups may happen. We need to test if we
                   ;; are blocked every time.
                   (unless (blocked?)
                     (go :ready))
                   (wait-for-ready connection)
                   (go :wakeup)
                 :ready
                   (chanl:select
                     ((chanl:recv request-queue request)
                      (let ((response (process-request connection request)))
                        (when response
                          (add-message-to-outbox connection response))))
                     ((chanl:recv outbox message)
                      (send-message-using-transport
                       transport connection message))
                     (otherwise
                      (error "This should not happen (one or the other queue should be ready).")))))
            ;; Broken pipe
            (stream-error ())))))))

(defgeneric run-reading-loop (transport connection)
  (:method ((transport transport) connection)
    (handler-case
        (loop for message = (receive-message-using-transport transport connection)
              while message
              do (add-message-to-queue connection message))
      ((or end-of-file stream-error) ()))))

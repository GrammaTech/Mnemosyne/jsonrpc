(defpackage #:jsonrpc/transport/stdio
  (:use #:cl
        #:jsonrpc/utils
        #:jsonrpc/transport/interface)
  (:import-from #:jsonrpc/connection
                #:connection
                #:connection-socket)
  (:import-from #:yason)
  (:import-from #:bordeaux-threads
                #:make-thread)
  (:import-from #:jsonrpc/utils
                #:destroy-thread*)
  (:import-from #:jsonrpc/request-response
                #:parse-message)
  (:import-from #:event-emitter
                #:emit)
  (:import-from :jsonrpc/class
                :jsonrpc-parser)
  (:import-from :babel
                :string-size-in-octets)
  (:import-from :uiop
                :*stdout*
                :*stdin*)
  (:export #:stdio-transport))
(in-package #:jsonrpc/transport/stdio)

(defclass stdio-transport (transport)
  ((input
    :type stream
    :initarg :input
    :initform *stdin*
    :accessor stdio-transport-input
    :documentation "The stream we read from (our input, their output).")
   (output
    :type stream
    :initarg :output
    :initform *stdout*
    :accessor stdio-transport-output
    :documentation "The stream we write to (our output, their input).")))

(defmethod find-mode-class ((mode (eql :stdio)))
  (find-class 'stdio-transport))

(defmethod start-server ((transport stdio-transport))
  (let* ((stream (make-two-way-stream (stdio-transport-input transport)
                                      (stdio-transport-output transport)))
         (connection (make-instance 'connection
                       :socket stream
                       :request-callback (transport-message-callback transport))))
    (setf (transport-connection transport) connection)
    (emit :open transport connection)
    (let ((thread
            (bt:make-thread
             (lambda ()
               (unwind-protect
                    (run-processing-loop transport connection)
                 (emit :close connection)))
             :name "SERVER jsonrpc/transport/stdio processing")))
      (unwind-protect* (run-reading-loop transport connection)
        (destroy-thread* thread)))))

(defmethod start-client ((transport stdio-transport))
  (let* ((stream (make-two-way-stream (stdio-transport-input transport)
                                      (stdio-transport-output transport)))
         (connection (make-instance 'connection
                                    :socket stream
                                    :request-callback (transport-message-callback transport))))
    (setf (transport-connection transport) connection)
    (emit :open transport connection)

    (setf (transport-threads transport)
          (list
           (bt:make-thread
            (lambda ()
              (run-processing-loop transport connection))
            :name (format nil "jsonrpc/transport/stdio writing ~a"
                          (stdio-transport-output transport)))
           (bt:make-thread
            (lambda ()
              (run-reading-loop transport connection))
            :name (format nil "jsonrpc/transport/stdio reading ~a"
                          (stdio-transport-input transport)))))
    connection))

(defmethod send-message-using-transport ((transport stdio-transport) connection message)
  (let ((json (with-output-to-string (s)
                (yason:encode message s)))
        (stream (connection-socket connection)))
    (format stream "Content-Length: ~A~C~C~:*~:*~C~C~A"
            (string-size-in-octets json :encoding :utf-8)
            #\Return
            #\Newline
            json)
    (finish-output stream)))

(defun read-utf8-length (input length)
  (flet ((utf8len (s)
           (string-size-in-octets s :encoding :utf-8)))
    (with-output-to-string (s)
      (loop for char = (read-char input)
            for char-len = (utf8len (string char))
            summing char-len into total
            do (assert (<= total length))
               (write-char char s)
            until (= total length)))))

(defmethod receive-message-using-transport ((transport stdio-transport) connection)
  (let* ((stream (connection-socket connection))
         (headers (read-headers stream))
         (length (ignore-errors (parse-integer (gethash "content-length" headers)))))
    (when length
      (let ((body
              (read-utf8-length (stdio-transport-input transport)
                                length)))
        (parse-message body
                       (jsonrpc-parser
                        (transport-jsonrpc transport)))))))

;; character stream
(defun read-headers (stream)
  (let ((headers (make-hash-table :test 'equal)))
    (loop for line = (read-line stream)
          until (equal (string-trim '(#\Return #\Newline) line) "")
          do (let* ((colon-pos (position #\: line))
                    (field (string-downcase (subseq line 0 colon-pos)))
                    (value (string-trim '(#\Return #\Space #\Tab) (subseq line (1+ colon-pos)))))
               (setf (gethash field headers) value)))
    headers))

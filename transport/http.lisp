(in-package #:cl-user)
(defpackage #:jsonrpc/transport/http
  (:use #:cl
        #:jsonrpc/utils
        #:jsonrpc/transport/interface
        #:jsonrpc/transport/tcp)
  (:import-from #:jsonrpc/connection
                #:connection
                #:connection-socket)
  (:import-from #:gt #:string+)
  (:import-from #:trivial-utf-8
                #:utf-8-bytes-to-string
                #:string-to-utf-8-bytes)
  (:export #:http-transport))
(in-package #:jsonrpc/transport/http)

(defclass http-transport (tcp-transport)
  ())

(defmethod find-mode-class ((mode (eql :http)))
  (find-class 'http-transport))

(defmethod send-message-using-transport :before ((transport http-transport)
                                                 connection
                                                 message)
  (let ((stream (connection-socket connection)))
    (flet ((write-header (s)
             (write-sequence
              (string-to-utf-8-bytes
               (format nil "~a~c~c" s #\Return #\Linefeed))
              stream)))
      ;; JSONRPC expects a persistent connection so specifying 1.1 is
      ;; important.
      (write-header "POST / HTTP/1.1")
      (write-header "Content-Type: application/json; charset=UTF-8"))))

(defmethod receive-message-using-transport :before ((transport http-transport)
                                                    connection)
  ;; Discard the status.
  (let ((stream (connection-socket connection)))
    (loop for byte = (read-byte stream 0)
          until (eql byte (char-code #\Linefeed)))))
